=== Atelier Scroll Top ===
Contributors: mariusz88atelierweb
Donate link: paypal.me/mariusz1988
Tags: Scroll, Scroll top, arrows, scroll to top, top , link to top
Requires at least: 4.0
Tested up to: 5.2.2
Requires PHP: 5.2.4
Stable tag: 1.1
Version: 1.1

Atelier Scroll Top is a simple plugin that takes you to the very top of your site ...

=== Description ===

This plugin in a quick and easy way allows the user, with one click, to return to the beginning of the scrolling page ....

= Features =
* Enabled plugin
* Select icon or write text
* Choose position and settings offset(top, bottom,right,left) in pixels
* Choose color icon
* Select shape border (rectangle,circle)
* Choose size border and select color
* Settings background color and background hover color
* Place in yours code css



== Installation ==

1. Upload the folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==

1. Atelier Scroll Top Settings page screenshot-01.png

== Changelog ==

= 1.0 =
* Initial release

= 1.1 =
* Refractoring code ,add settings border hover color

= 1.2 =
* Select the scroll view on selected pages and post pages.